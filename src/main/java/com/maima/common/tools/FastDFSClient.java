package com.maima.common.tools;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import org.apache.commons.lang3.StringUtils;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.StorageClient1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


/**
 * FastDFS分布式文件系统操作客户端.
 *
 * @author WuShuicheng .
 */
public class FastDFSClient {
    private static StorageClient1 storageClient = null;

    private static Logger logger = LoggerFactory.getLogger(FastDFSClient.class);

    /**
     * 只加载一次.
     */
    static {
    }

    /**
     * @param file     文件
     * @param fileName 文件名
     * @return 返回Null则为失败
     */
    public static String uploadFile(MultipartFile file, String fileName) {
//		System.out.println("图片服务器配置："+CONF_FILENAME);
        InputStream fis = null;
        try {
            NameValuePair[] meta_list = null; // new NameValuePair[0];
            fis = file.getInputStream();
            byte[] file_buff = null;
            if (fis != null) {
                int len = fis.available();
                file_buff = new byte[len];
                fis.read(file_buff);
            }
            String fileid = storageClient.upload_file1(file_buff, getFileExt(fileName), meta_list);
            return fileid;
        } catch (Exception ex) {
            logger.error("加载异常:{}", ex.toString());
            return null;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("加载异常:{}", e);
                }
            }
        }
    }

    /**
     // 创建存储桶
     // 用户确定地域和存储桶名称后，参考如下示例创建存储桶
     String bucket = "examplebucket-1250000000"; //存储桶名称，格式：BucketName-APPID
     CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucket);
     // 设置 bucket 的权限为 Private(私有读写), 其他可选有公有读私有写, 公有读写
     createBucketRequest.setCannedAcl(CannedAccessControlList.Private);
     try{
     Bucket bucketResult = cosClient.createBucket(createBucketRequest);
     } catch (CosServiceException serverException) {
     serverException.printStackTrace();
     } catch (CosClientException clientException) {
     clientException.printStackTrace();
     }


     // 查询存储桶列表
     // 查询用户的存储桶列表，参考示例如下：
     List<Bucket> buckets = cosClient.listBuckets();
     for (Bucket bucketElement : buckets) {
     String bucketName = bucketElement.getName();
     String bucketLocation = bucketElement.getLocation();


     // 下载对象
     // 上传对象后，您可以用同样的 key，调用 GetObject 接口将对象下载到本地，也可以生成预签名链接（下载请指定 method 为 GET，详情请参见 预签名 URL），分享到其他终端来进行下载。但如果您的文件设置了私有读权限，那么请注意预签名链接的有效期。
     // 将文件下载到本地指定路径，参考示例如下
     //             Bucket的命名格式为 BucketName-APPID ，此处填写的存储桶名称必须为此格式
     //            String bucketName = "examplebucket-1250000000";
     //            String key = "exampleobject";
     // 方法1 获取下载输入流
     GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);
     //            COSObject cosObject = cosClient.getObject(getObjectRequest);
     //            COSObjectInputStream cosObjectInput = cosObject.getObjectContent();
     // 方法2 下载文件到本地
     String outputFilePath = "C:\\Users\\ASUS\\Desktop\\exampleobject.jpg";
     File downFile = new File(outputFilePath);
     getObjectRequest = new GetObjectRequest(bucketName, key);
     ObjectMetadata downObjectMeta = cosClient.getObject(getObjectRequest, downFile);
     System.out.println("downObjectMeta - "+JSONObject.toJSONString(downObjectMeta));
     }
     */
    /**
     * APPID：
     * APPKEY：
     * Bucket:
     */
    public static String updateLoadFileToOss(String userId, MultipartFile file, String fileName) {//File file
        String Url="https://<bucketName>-<appId>.cos.ap-shanghai.myqcloud.com/"+fileName;
        // 1 初始化用户身份信息（secretId, secretKey）。
        String secretId = "";        //COS_SECRETID
        String secretKey = "";           //COS_SECRETKEY
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的区域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region("ap-shanghai");   //COS_REGION
        ClientConfig clientConfig = new ClientConfig(region);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);

        InputStream fis = null;
        try {
            fis = file.getInputStream();
//            FileInputStream fileInputStream = new FileInputStream(file);
//            fis=fileInputStream;

            // 指定要上传的文件
//            File localFile = new File(file.getAbsolutePath());//localFilePath  file.getOriginalFilename()
            File localFile = new File(file.getOriginalFilename());//localFilePath
            inputStreamToFile(fis, localFile);
            // 指定要上传到的存储桶
            String bucketName = "";
            // 指定要上传到 COS 上对象键
            String key = "staticresources";
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            // 关闭客户端(关闭后台线程)
            cosClient.shutdown();
        } catch (Exception ex) {
            logger.error("加载异常:{}", ex.toString());
            return null;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("加载异常:{}", e);
                }
            }
        }
        return Url;
    }

    public static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
























    /**
     * 根据组名和远程文件名来删除一个文件
     *
     * @param groupName 例如 "group1" 如果不指定该值，默认为group1
     * @param fileName  例如"M00/00/00/wKgxgk5HbLvfP86RAAAAChd9X1Y736.jpg"
     * @return 0为成功，非0为失败，具体为错误代码
     */
    public static int deleteFile(String groupName, String fileName) {
        try {
            int result = storageClient.delete_file(groupName == null ? "group1" : groupName, fileName);
            return result;
        } catch (Exception ex) {
            logger.error("加载异常:{}", ex);
            return -1;
        }
    }

    /**
     * 根据fileId来删除一个文件（我们现在用的就是这样的方式，上传文件时直接将fileId保存在了数据库中）
     *
     * @param fileId file_id源码中的解释file_id the file id(including group name and filename);例如 group1/M00/00/00/ooYBAFM6MpmAHM91AAAEgdpiRC0012.xml
     * @return 0为成功，非0为失败，具体为错误代码
     */
    public static int deleteFile(String fileId) {
        try {
            int result = storageClient.delete_file1(fileId);
            return result;
        } catch (Exception ex) {
            logger.error("加载异常:", ex);
            return -1;
        }
    }

    /**
     * 修改一个已经存在的文件
     *
     * @param oldFileId 原来旧文件的fileId, file_id源码中的解释file_id the file id(including group name and filename);例如 group1/M00/00/00/ooYBAFM6MpmAHM91AAAEgdpiRC0012.xml
     * @param file      新文件
     * @param fileName  文件名
     * @return 返回空则为失败
     */
    public static String modifyFile(String userId, String oldFileId, MultipartFile file, String fileName) {
        String fileid = null;
        try {
            // 先上传
//            fileid = uploadFile(file, fileName);
//            fileid = updateLoadFileToOss(userId, file, fileName);
            if (fileid == null) {
                logger.info("上传图片后fileid为空");
                return null;
            }
            // 再删除
            int delResult = deleteFile(oldFileId);
            if (delResult != 0) {
                logger.info("删除图片后删除结果不为0");
                return fileid;
            }
        } catch (Exception ex) {
            logger.error("加载异常:{}", ex);
            return null;
        }
        return fileid;
    }

    /**
     * 文件下载
     *
     * @param fileId
     * @return 返回一个流
     */
    public static InputStream downloadFile(String fileId) {
        try {
            byte[] bytes = storageClient.download_file1(fileId);
            InputStream inputStream = new ByteArrayInputStream(bytes);
            return inputStream;
        } catch (Exception ex) {
            logger.error("加载异常:", ex);
            return null;
        }
    }

    /**
     * 获取文件后缀名（不带点）.
     *
     * @return 如："jpg" or "".
     */
    private static String getFileExt(String fileName) {
        if (StringUtils.isBlank(fileName) || !fileName.contains(".")) {
            return "";
        } else {
            return fileName.substring(fileName.lastIndexOf(".") + 1); // 不带最后的点
        }
    }
}
