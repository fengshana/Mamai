package com.maima.common.tools;

import java.util.UUID;

public class CommonUtil {
    public static String getUUID() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        return uuid;
    }
}
