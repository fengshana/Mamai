package com.maima.common.tools.exception;

public class BizException extends RuntimeException {
    /**
     * 数据库操作,selectOne返回null
     */
    public static final BizException DB_SELECTONE_IS_NULL = new BizException("DB01", "数据库操作,selectOne返回null");
    /**
     * 生成序列异常时
     */
    public static final BizException DB_GET_SEQ_NEXT_VALUE_ERROR = new BizException("DB02", "序列生成超时");

    /**
     * 参数不存在的公共异常
     */
    public static final BizException ORDER_EXIST_ERROR = new BizException("O001", "订单已存在");
    public static final BizException ORDER_NOT_EXIST_ERROR = new BizException("O002", "订单不存在");
    public static final BizException TRANS_SN_IS_NULL = new BizException("O003", "transSn 为空");
    public static final BizException MERCH_NO_IS_NULL = new BizException("O004", "merchNo 为空");
    public static final BizException TRANS_AMT_IS_NULL = new BizException("O005", "transAmt 为空");
    public static final BizException CARD_NO_IS_NULL = new BizException("O006", "cardNo 为空");
    public static final BizException CHANNEL_ID_IS_NULL = new BizException("O007", "channelId 为空");
    public static final BizException TRANS_TYPE_IS_NULL = new BizException("O008", "transType 为空");
    public static final BizException SETT_DATE_IS_NULL = new BizException("O009", "settDate 为空");
    public static final BizException CHNL_ORDER_ID_IS_NULL = new BizException("O010", "channelOrderId 为空");
    public static final BizException ORDER_HAD_BEAN_DO_ = new BizException("O011", "订单状态非初始状态");
    public static final BizException BUS_CODE_IS_NULL = new BizException("O012", "busCode为空");
    public static final BizException MSG_TYPE_IS_NULL = new BizException("O013", "参数MSG_TYPE为空");

    public static final BizException TRANS_IS_NULL = new BizException("O013", "trans为空");
    public static final BizException COLECT_PAY_IS_NULL = new BizException("O014", "collectpayOrder为空");
    public static final BizException BUS_IS_OPEN = new BizException("O015", "业务已经开通过");
    public static final BizException TRANSTYPE_NOT_EXIT = new BizException("O016", "交易类型不存在");
    public static final BizException TRANS_IS_PROCESS = new BizException("O017", "上一笔支付处理中，请5分钟后再尝试");
    public static final BizException MERCHLEVEL_IS_NULL = new BizException("O018", "商户等级为空");
    public static final BizException MERCHLEVEL_IS_LOWER = new BizException("O019", "商户等级大于要升级的等级");
    public static final BizException SMS_CODE_IS_NULL = new BizException("O020", "smsCode为空");
    public static final BizException CHANNEL_CODE_IS_NULL = new BizException("O021", "channelcode为空");
    public static final BizException DATE_FORMAT_ERROR = new BizException("O021", "日期格式错误");
    public static final BizException PAGE_URL_IS_NULL = new BizException("O022", "PAGE_URL为空");
    public static final BizException REG_ID_NOT_EXIT = new BizException("O023", "regId为空");

    public static final BizException SYSTEM_ERROR = new BizException("S999", "系统异常");
    public static final BizException CHANNEL_SYSTEM_ERROR = new BizException("S998", "渠道系统异常");

    public static final BizException CHANNEL_REGISTER_DEF_NOT_EXIT = new BizException("B001", "渠道进件信息配置不存在");
    public static final BizException CHANNEL_REGISTER_DEF_NOT_VALID = new BizException("B002", "渠道进件信息配置未生效");
    public static final BizException FUNCTION_NOT_AVAILABLE = new BizException("B003", "暂不支持该业务");
    public static final BizException ROUTE_NOT_AVAILABLE = new BizException("B004", "交易暂无渠道，请稍后重试");
    public static final BizException SMSCODE_NOT_VALID = new BizException("B005", "短信验证码不正确，请重试");

    public static final BizException ORIGORDERID_NOT_EXIT = new BizException("B006", "源订单不存在");
    public static final BizException CHANNEL_ORDER_INFO_EMPTY = new BizException("B007", "渠道交易信息为空");
    public static final BizException SMS_CODE_IS_NOT_EQUALS_ORIG = new BizException("B008", "短信验证码与原订单不一致");
    public static final BizException ORIG_ORDER_IS_FAIL = new BizException("B009", "原订单交易失败");
    public static final BizException CHECK_TYPE_IS_ERROR = new BizException("B010", "checkType类型有误");
    public static final BizException BRANCH_IS_UNUSED = new BizException("B011", "代理商已停用");
    public static final BizException AMT_IS_UNUSED = new BizException("B012", "交易金额与原订单不一致");

    public static final BizException MERCH_NOT_EXIT = new BizException("B013", "商户不存在");
    public static final BizException MERCH_CARD_NOT_EXIT = new BizException("B013", "商户未绑定该卡");


    public static final BizException TRANSSN_IS_NULL = new BizException("F001", "transSn为空");
    public static final BizException TRANS_NOT_EXIT = new BizException("F002", "订单不存在");

    /**
     * 具体异常码
     */
    protected String code;
    /**
     * 异常信息
     */
    protected String msg;

    public BizException(String code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
        this.msg = String.format(msgFormat, args);
    }

    public BizException() {
        super();
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message) {
        super(message);
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "BizException{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    /**
     * 实例化异常
     *
     * @param msgFormat
     * @param args
     * @return
     */
    public BizException newInstance(String msgFormat, Object... args) {
        return new BizException(this.code, msgFormat, args);
    }
}
