package com.maima.common.tools;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class FileUtil {
    public static final List<String> IMAGE_EXTENSIONS = Arrays.asList(".jpg", ".jpeg", ".png",".gif");
}
