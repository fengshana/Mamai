package com.maima.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.maima.common.tools.*;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
public class FileServiceImpl{
    private String SERVER_URL="http://127.0.0.1:8080";

    //文件上传
    public JSONObject uploadFile(MultipartFile file) throws IOException {
        String filePath = null;
        JSONObject result=new JSONObject();
        boolean flag=true;
        if(flag){ //传入oss
            /**
             * 图片压缩
             Long imgMaxSize = 1024L * 1024L;
             List<String> imgsType = new ArrayList<>(Arrays.asList("BMP","JPG","PNG"));
             String fileName = file.getOriginalFilename();
             Long fileSize = file.getSize();
             String fileType = file.getContentType().toUpperCase();
             InputStream inputStream = file.getInputStream();
             //如果上传的是图片，且超过指定大小，自动压缩输入流
             if(imgsType.contains(fileType)&&fileSize>imgMaxSize){
             ByteArrayOutputStream out = new ByteArrayOutputStream();
             //压缩输入流，scale是压缩比例；quality是质量比例，0-1之间，越接近1,质量越高
             Thumbnails.of(inputStream).scale(0.7f).outputQuality(0.25d).toOutputStream(out);
             InputStream imgInputStream = new ByteArrayInputStream(out.toByteArray());
             inputStream = imgInputStream;
             }
             //保存文件到本地服务器
             String dirPath = "/static/upload/";
             File dir = new File(dirPath);
             if(!dir.exists()){
             dir.mkdirs();
             }
             File newFile = new File(dirPath+fileName);
             newFile.createNewFile();
             FileOutputStream outStream = new FileOutputStream(newFile);
             int len;
             byte[] buffer = new byte[1024];
             while ((len = inputStream.read(buffer)) != -1) {
             outStream.write(buffer, 0, len);
             }
             outStream.close();
             */
            String imageName = file.getOriginalFilename();
            if (!imageName.contains(".")) {
                System.out.println("uploadFile - 图片名称不含后缀，手动加入");
                imageName = imageName + ".jpg";
            }
//            imageName =  Kits.Date.getyyyyMMddHHmmss() + Kits.Random.getRandom(0, 10000) + imageName;
            filePath = FastDFSClient.updateLoadFileToOss(imageName, file, imageName);//新增相片到图片服务器
            if (StringUtils.isEmpty(filePath)) {
                System.out.println("图片上传失败");
                result.put("desc","上传失败，请重试");
                return result;
            }
            System.out.println("照片路径："+ filePath + "，图片名称：" + imageName);
        }else{ //传入本地服务，并
            // 源文件名称
            final String originalFileName = file.getOriginalFilename();
            if (StringUtils.isBlank(originalFileName)) {
                System.out.println("请选择图片！");
                result.put("desc","请选择图片！");
                return result;
            }
            // 文件后缀[.jpg]
            final String suffix = originalFileName.substring(originalFileName.lastIndexOf(".")).toLowerCase();
            if (!FileUtil.IMAGE_EXTENSIONS.contains(suffix)) {
                System.out.println("图片格式错误！");
                result.put("desc","图片格式错误！");
                return result;
            }

            String newFileName = CommonUtil.getUUID()+suffix;
            String relativePath =  DateUtil.getYYYYMMDD() +"/" + DateUtil.getHH();
            File upload=null;
            try {
                File path = new File(ResourceUtils.getURL("classpath:").getPath());
                if(!path.exists()) path = new File("");
                System.out.println("path.getAbsolutePath:[{}] "+ path.getAbsolutePath());
                upload= new File(path.getAbsolutePath(),"/static/upload");
                if(!upload.exists()) upload.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
            filePath = upload.getPath() +"/"+ relativePath;
            System.out.println("uploadFile - filePath:[{}] "+ filePath);
            File targetFile = new File(filePath);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            FileOutputStream out = null;
            try {
                String lastFilePath = filePath + "/" + newFileName;
                out = new FileOutputStream(lastFilePath);
                out.write(file.getBytes());
                filePath = SERVER_URL+"/static/"+ upload.getName() + "/" + relativePath + "/" + newFileName;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (filePath == null) {
                System.out.println("文件上传失败！");
                result.put("desc","文件上传失败！");
                return result;
            }
            System.out.println("照片路径：" + filePath);
        }
        System.out.println("==================文件上传成功==================");
        result.put("filePath", filePath);
        result.put("desc","文件上传成功");
        return result;
    }




    private void uploadFileLimitSize(MultipartFile file) throws IOException{
        Long imgMaxSize = 1024L * 1024L;
        List<String> imgsType = new ArrayList<>(Arrays.asList("BMP","JPG","PNG"));
                String fileName = file.getOriginalFilename();
                Long fileSize = file.getSize();
                String fileType = file.getContentType().toUpperCase();
                InputStream inputStream = file.getInputStream();
                //如果上传的是图片，且超过指定大小，自动压缩输入流
                if(imgsType.contains(fileType)&&fileSize>imgMaxSize){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    //压缩输入流，scale是压缩比例；quality是质量比例，0-1之间，越接近1,质量越高
                    Thumbnails.of(inputStream).scale(0.7f).outputQuality(0.25d).toOutputStream(out);
                    InputStream imgInputStream = new ByteArrayInputStream(out.toByteArray());
                    inputStream = imgInputStream;
                }
                //保存文件到本地服务器
                String dirPath = "/src/img/";
                File dir = new File(dirPath);
                if(!dir.exists()){
                    dir.mkdirs();
                }
                File newFile = new File(dirPath+fileName);
                newFile.createNewFile();
                FileOutputStream outStream = new FileOutputStream(newFile);
                int len;
                byte[] buffer = new byte[1024];
                while ((len = inputStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }
                outStream.close();
    }
}
