package com.maima.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maima.common.tools.exception.BizException;
import com.maima.entity.MaiMa;
import com.maima.mapper.MaiMaMapper;
import com.maima.service.MaimaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
@Service
public class MaiMaServiceImpl extends ServiceImpl<MaiMaMapper, MaiMa> implements MaimaService {

    @Autowired
    MaiMaMapper maiMaMapper;

    @Override
//    @Transactional
    public int testAdd() throws BizException {
        System.out.println("========= [testAdd] =========");
        JSONObject resultJSON = new JSONObject();
        try {
            MaiMa insertMaiMa= new MaiMa();
            insertMaiMa.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            insertMaiMa.setNumber(1111111);
            insertMaiMa.setOneColumn("1");
            insertMaiMa.setTwoColumn("1");
            insertMaiMa.setThreeColumn("1");
            insertMaiMa.setFourColumn("1");
            insertMaiMa.setFiveColumn("1");
            insertMaiMa.setSixColumn("1");
            insertMaiMa.setSevenColumn("1");
            insertMaiMa.setStatus(0);
            int n=maiMaMapper.addMaiMa(insertMaiMa);
            if(n>0){
                System.out.println("addMaiMa - 新增成功： "+n);
                resultJSON.put("Msg","新增成功");
                resultJSON.put("Text","新增成功");
                resultJSON.put("Status","ok");
                resultJSON.put("Code",200);
            }else{
                System.out.println("addMaiMa - 新增失败： "+n);
                resultJSON.put("Msg","新增失败");
                resultJSON.put("Text","新增失败");
                resultJSON.put("Status","fail");
                resultJSON.put("Code",001);
            }
            int i = 1/0;
        }catch (ArithmeticException e){
            System.out.println("[testAdd] - 抛出异常");
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//关键
            throw new BizException("0001","方法抛出异常信息 - ArithmeticException！");
        }
        System.out.println("resultJSON - "+resultJSON.toString());
        return 0;
    }
}
