package com.maima.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maima.entity.TUserInfo;
import com.maima.mapper.TUserInfoMapper;
import com.maima.service.TUserInfoService;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
@Service
//@Transactional(rollbackFor = Exception.class)
public class TUserInfoServiceImpl extends ServiceImpl<TUserInfoMapper, TUserInfo> implements TUserInfoService {

    @Override
    public int testAdd1() {
        return 1/0;
    }
}
