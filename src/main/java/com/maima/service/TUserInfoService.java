package com.maima.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.maima.entity.TUserInfo;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
public interface TUserInfoService extends IService<TUserInfo> {
    int testAdd1();

}
