package com.maima.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.maima.entity.MaiMa;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
public interface MaimaService extends IService<MaiMa> {


    int testAdd();

}
