package com.maima.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.maima.entity.MaiMa;
import com.maima.entity.RequestDataBean;
import com.maima.entity.TUserInfo;
import com.maima.mapper.MaiMaMapper;
import com.maima.mapper.TUserInfoMapper;
import com.maima.service.Impl.FileServiceImpl;
import com.maima.service.MaimaService;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/maima")
public class MaiMaContorller {

    @Autowired
    private MaiMaMapper maiMaMapper;
    @Autowired
    private TUserInfoMapper tUserInfoMapper;
    @Autowired
    FileServiceImpl fileService;



    @Autowired
    MaimaService maimaService;

    @RequestMapping("/testAdd")
    public int testAdd(){
        System.out.println("[testAdd] ---------");
        return maimaService.testAdd();
    }


    @RequestMapping("/getFreight")
    public void getFreight(String sku, JSONArray skuJSONArray){
        System.out.println("[getFreight] - {sku}:" + sku);
        System.out.println("[getFreight] - {skuJSONArray}:" + skuJSONArray.toJSONString());
    }

    @RequestMapping(value = "/getFreight2", method = RequestMethod.POST)
    public void getFreight2(@RequestBody JSONArray skuJSONArray){
        System.out.println("[getFreight] - {skuJSONArray}:" + skuJSONArray.toJSONString());
    }

    /**
     * @param file
     * @return
     * @Description 上传文件
     */
    @ApiOperation(value = "上传文件", notes = "上传照片接口")
    @ApiImplicitParams({
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功"),
            @ApiResponse(code = 400, message = "失败")
    })
    @RequestMapping(value = "uploadFile", method = {RequestMethod.POST})
    @ResponseBody
    public JSONObject uploadFile(
            @RequestParam(value = "file", required = true) MultipartFile file) throws IOException {
        System.out.println("接收到上传文件请求uploadFile");
        if (file == null) {
            System.out.println("=================文件为空=================");
        }
        return fileService.uploadFile(file);
    }

    @PostMapping("/list")
    @ResponseBody
    public List<MaiMa> list(){
        List<MaiMa> list = maiMaMapper.list();
        System.out.println("list - listData:[{}],"+ JSONArray.toJSONString(list));
        return list;
    }

    @PostMapping("/addMaiMa")
    @ResponseBody
    public JSONObject addMaiMa(RequestDataBean requestDataBean){
        System.out.println("=================addMaiMa====================");
        System.out.println("requestDataBean: "+JSONObject.toJSONString(requestDataBean));
        JSONObject resultJSON=new JSONObject();
        boolean flag=checkParamValid(requestDataBean);
        if(!flag){
            resultJSON.put("Msg","参数缺少，请检查参数并重新上送");
            resultJSON.put("Text","参数缺少，请检查参数并重新上送");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
            return resultJSON;
        }
        MaiMa insertMaiMa=JSONObject.parseObject(JSONObject.toJSONString(requestDataBean),MaiMa.class);
        insertMaiMa.setNumber(requestDataBean.getNumberId());
        insertMaiMa.setStatus(0);
        int n=maiMaMapper.addMaiMa(insertMaiMa);
        if(n>0){
            System.out.println("addMaiMa - 新增成功： "+n);
            resultJSON.put("Msg","新增成功");
            resultJSON.put("Text","新增成功");
            resultJSON.put("Status","ok");
            resultJSON.put("Code",200);
        }else{
            System.out.println("addMaiMa - 新增失败： "+n);
            resultJSON.put("Msg","新增失败");
            resultJSON.put("Text","新增失败");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
        }
        return resultJSON;
    }


    @PostMapping("/editMaiMa")
    @ResponseBody
    public JSONObject editMaiMa(RequestDataBean requestDataBean){
        System.out.println("=================editMaiMa====================");
        System.out.println("requestDataBean: "+JSONObject.toJSONString(requestDataBean));
        JSONObject resultJSON=new JSONObject();
        if(requestDataBean.getId()==null){
            resultJSON.put("Msg","参数缺少，请检查参数并重新上送");
            resultJSON.put("Text","参数缺少，请检查参数并重新上送");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
            return resultJSON;
        }
        boolean flag=checkParamValid(requestDataBean);
        if(!flag){
            resultJSON.put("Msg","参数缺少，请检查参数并重新上送");
            resultJSON.put("Text","参数缺少，请检查参数并重新上送");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
            return resultJSON;
        }
        MaiMa insertMaiMa=JSONObject.parseObject(JSONObject.toJSONString(requestDataBean),MaiMa.class);
        if(requestDataBean.getNumberId()!=null){
        insertMaiMa.setNumber(requestDataBean.getNumberId());
        }
        int n=maiMaMapper.updateById(insertMaiMa);
        if(n>0){
            System.out.println("addMaiMa - 编辑成功： "+n);
            resultJSON.put("Msg","编辑成功");
            resultJSON.put("Text","编辑成功");
            resultJSON.put("Status","ok");
            resultJSON.put("Code",200);
        }else{
            System.out.println("addMaiMa - 编辑失败： "+n);
            resultJSON.put("Msg","编辑失败");
            resultJSON.put("Text","编辑失败");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
        }
        return resultJSON;
    }


    public boolean checkParamValid(RequestDataBean requestDataBean){
        if(requestDataBean.getNumberId()==null){
            System.out.println("========期数为空========");
           return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getOneColumn())){
            System.out.println("========第一位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getTwoColumn())){
            System.out.println("========第二位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getThreeColumn())){
            System.out.println("========第三位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getFourColumn())){
            System.out.println("========第四位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getFiveColumn())){
            System.out.println("========第五位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getSixColumn())){
            System.out.println("========第六位数为空========");
            return false;
        }
        if(StringUtils.isEmpty(requestDataBean.getSevenColumn())){
            System.out.println("========期数为空========");
            return false;
        }
        return true;
    }



    @PostMapping("/findAllByParams")
    @ResponseBody
    public JSONObject findAllByParams(RequestDataBean requestDataBean){
        System.out.println("findAllByParams - requestBean: "+JSONObject.toJSONString(requestDataBean));
        if(requestDataBean.getPageSize()==null || requestDataBean.getPage()<1){
            requestDataBean.setPageSize(100);
        }
        if(requestDataBean.getPage()==null || requestDataBean.getPage()<1){
            requestDataBean.setPage(1);
        }
        if(StringUtils.isNotEmpty(requestDataBean.getNumber())){
            String[] strings=requestDataBean.getNumber().split(",");
            List<Integer> numbers=new ArrayList<Integer>();
            for (String num:strings){
                numbers.add(Integer.parseInt(num));
            }
            requestDataBean.setNumbers(numbers);
        }
        if(StringUtils.isNotEmpty(requestDataBean.getCreateTime1()) && StringUtils.isNotEmpty(requestDataBean.getCreateTime2())){
            requestDataBean.setType(1);
        }else{
            requestDataBean.setType(0);
        }
        int count = maiMaMapper.findAll(requestDataBean).size();//总条数
        int page=count%requestDataBean.getPageSize()==0?count/requestDataBean.getPageSize():count/requestDataBean.getPageSize()+1;//最大页数

        RequestDataBean requestDataBean1=JSONObject.parseObject(JSONObject.toJSONString(requestDataBean),RequestDataBean.class);
        if(requestDataBean.getPage()>page){
            requestDataBean.setPage(page);
        }
        if(requestDataBean.getPage()<1){
            requestDataBean.setPage(1);
        }
        requestDataBean1.setPage((requestDataBean.getPage()-1)*requestDataBean.getPageSize());
        System.out.println("params:[{}] "+JSONObject.toJSONString(requestDataBean1));
        List<MaiMa> list = maiMaMapper.findAllByParams(requestDataBean1);
        System.out.println("list - listData:[{}] "+ JSONArray.toJSONString(list));

        List<MaiMa> list3=maiMaMapper.findAllKeys(requestDataBean);
        JSONObject resultJSON=new JSONObject();
        resultJSON.put("list",list);//分页数据
        resultJSON.put("list2",list3);
        resultJSON.put("sumPage",page);
        resultJSON.put("page",requestDataBean.getPage());
        resultJSON.put("pageSize",requestDataBean.getPageSize());
        System.out.println("[response data]:[{}], "+resultJSON.toString());
        return resultJSON;
    }








    @RequestMapping("/list2")
    public String list2(){
        return "customer";
    }

    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("/login");
        return mav;
    }



    @PostMapping("/login")
    @ResponseBody
    public JSONObject login(HttpServletRequest request) {
        String username=request.getParameter("login");
        String password=request.getParameter("pwd");
        String code=request.getParameter("code");

        System.out.println("username: "+username);
        System.out.println("password: "+password);
        System.out.println("code: "+code);

        Map params=new HashMap();
        params.put("username",username);
        params.put("password",password);
        TUserInfo tUserInfo=tUserInfoMapper.login(params);
        JSONObject resultJSON=new JSONObject();
        if(tUserInfo!=null){
            request.getSession().setAttribute("tUserInfo",tUserInfo);
            System.out.println("=== username:  [ "+username+" ]登陆成功 ===");
            resultJSON.put("Msg","登录成功");
            resultJSON.put("Text","登录成功");
            resultJSON.put("Status","ok");
            resultJSON.put("Code",200);
        }else{
            System.out.println("=== username:  [ "+username+" ]登陆失败 ===");
            resultJSON.put("Msg","登录失败，请检查账户或密码是否正确");
            resultJSON.put("Text","登录失败，请检查账户或密码是否正确");
            resultJSON.put("Status","fail");
            resultJSON.put("Code",001);
        }
        return resultJSON;
    }
}
