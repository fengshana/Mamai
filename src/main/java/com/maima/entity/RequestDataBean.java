package com.maima.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RequestDataBean {
    private Integer type;
    private Integer id;
    private Integer page;
    private Integer pageSize;
    private String number;//还可以用逗号隔开的那种
    private List<Integer> numbers;

    private Integer numberId;
    /**
     * 第一数
     */
    private String oneColumn;

    /**
     * 第二数
     */
    private String twoColumn;

    /**
     * 第三数
     */
    private String threeColumn;

    /**
     * 第四数
     */
    private String fourColumn;

    /**
     * 第五数
     */
    private String fiveColumn;

    /**
     * 第六数
     */
    private String sixColumn;

    /**
     * 第七数
     */
    private String sevenColumn;

    /**
     * 时间
     */
    private String createTime1;
    private String createTime;
    private String createTime2;
}
