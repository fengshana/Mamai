package com.maima.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class MaiMa {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;


    private Integer number;
    /**
     * 第一数
     */
    private String oneColumn;

    /**
     * 第二数
     */
    private String twoColumn;

    /**
     * 第三数
     */
    private String threeColumn;

    /**
     * 第四数
     */
    private String fourColumn;

    /**
     * 第五数
     */
    private String fiveColumn;

    /**
     * 第六数
     */
    private String sixColumn;

    /**
     * 第七数
     */
    private String sevenColumn;

    /**
     * 时间
     */
    private String createTime;

    /**
     * 状态
     */
    private Integer status;

}
