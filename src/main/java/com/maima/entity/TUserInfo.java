package com.maima.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class TUserInfo {

    private static final long serialVersionUID = 1L;

    /*用户编号*/
    @TableId(type = IdType.AUTO)
    private  Integer  id;
    /*此条数据的创建时间*/
    private  Long  createDate;
    /*此条数据的最后更新时间*/
    private  Long  lastUpdateDate;
    /*此条数据的创建人*/
    private  String  createBy;
    /*此条数据的最后更新人*/
    private  String  lastUpdateBy;
    /*此条数据的版本号*/
    private  Integer  objectVersionNumber;
    /*用户名称*/
    private  String  userName;
    /*用户手机号码*/
    private  String  userPhone;
    /*用户账号*/
    private  String  acctNo;
    /*用户密码，加密处理*/
    private  String  passWord;
    /*用户账号状态（0正常，1停用，2删除）,取自t_status_enum主键*/
    private  Integer  statusId;
    /*用户最后一次登录IP*/
    private  String  lastLoginIp;
    /*用户最后一次登录时间*/
    private  Long  lastLoginTime;
    /*用户登录时验证码*/
    private  String  vertifyCode;
    /*用户当前的角色，t_user_role_info的主键*/
    private  Integer  roleId;

}
