package com.maima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maima.entity.MaiMa;
import com.maima.entity.RequestDataBean;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
@Repository
public interface MaiMaMapper extends BaseMapper<MaiMa> {

    List<MaiMa> list();
    List<MaiMa> findAllByParams(RequestDataBean requestDataBean);
    List<MaiMa> findAllKeys(RequestDataBean requestDataBean);
    List<MaiMa> findAll(RequestDataBean requestDataBean);
    // 返回主键字段id值
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert(" insert into mai_ma\n" +
            "(id,number,one_column,two_column,three_column,four_column,five_column,six_column,seven_column,create_time,status)\n" +
            "values\n" +
            "(#{id},#{number},#{oneColumn},#{twoColumn},#{threeColumn},#{fourColumn},#{fiveColumn},#{sixColumn},#{sevenColumn},#{createTime},#{status})")
    Integer addMaiMa(MaiMa maiMa);
}
