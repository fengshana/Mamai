package com.maima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maima.entity.MaiMa;
import com.maima.entity.TUserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Mr_Zhou
 * @since 2019-10-28
 */
@Repository
public interface TUserInfoMapper extends BaseMapper<TUserInfo> {

    TUserInfo login(Map params);
}
