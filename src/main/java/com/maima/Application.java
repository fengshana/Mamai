package com.maima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 *
 * @author Mr_Zhou
 * @date 2020/01/6 17:10
 */

/**
 * 加@EnableTransactionManagement
 * 由于SpringBoot会自动配置事务，
 * 所以这个注解可加也不加，
 * 具体实现可在org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration类中查看。
 */
//启用事务管理（可省略）
//@EnableTransactionManagement
@SpringBootApplication()
@EnableScheduling
@MapperScan("com.maima.mapper")
public class Application  extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    // 不重写打包war部署到tomcat接口会报404
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}


