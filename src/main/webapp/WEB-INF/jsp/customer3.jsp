<%@ page import="com.alibaba.fastjson.JSONObject" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.maima.entity.MaiMa" %>
<%@ page import="com.maima.entity.TUserInfo" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="itcast" uri="http://itcast.cn/common/"%>
<%
	TUserInfo tUserInfo=(TUserInfo) request.getSession().getAttribute("tUserInfo");
	if(tUserInfo==null){
		String path = request.getContextPath();
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
		response.sendRedirect(basePath+"maima/index");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content=
	<!-- Bootstrap Core CSS -->
	<link href="/static/css/bootstrap.min.css" rel="stylesheet">
	<link href="/static/css/metisMenu.min.css" rel="stylesheet">
	<link href="/static/css/dataTables.bootstrap.css" rel="stylesheet">
	<link href="/static/css/sb-admin-2.css" rel="stylesheet">
	<link href="/static/css/font-awesome.min.css" rel="stylesheet"
		  type="text/css">
	<link href="/static/css/boot-crm.css" rel="stylesheet"
		  type="text/css">
	<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style>
		.inputText{
			width:200px;
		}
		.alert {
			display: none;
			position: fixed;
			top: 50%;
			left: 50%;
			min-width: 300px;
			max-width: 600px;
			transform: translate(-50%,-50%);
			z-index: 99999;
			text-align: center;
			padding: 15px;
			border-radius: 3px;
		}

		.alert-success {
			color: #3c763d;
			background-color: #dff0d8;
			border-color: #d6e9c6;
		}

		.alert-info {
			color: #31708f;
			background-color: #d9edf7;
			border-color: #bce8f1;
		}

		.alert-warning {
			color: #8a6d3b;
			background-color: #fcf8e3;
			border-color: #faebcc;
		}

		.alert-danger {
			color: #a94442;
			background-color: #f2dede;
			border-color: #ebccd1;
		}
	</style>
</head>
<body>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">编辑</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<div class="form-group">
						<label for="number">编号 </label><input  class="form-control inputText" type="text" id="id" readonly="readonly" />
						<label for="number">期数 </label><input  class="form-control inputText" type="text" id="numberId" />
						<label for="number">开奖日期 </label>
						<div class='input-group date'  style="width: 200px;"  id='datetimepicker3' >
						<input type='text' class="form-control" id='createTime'/>
						<span class="input-group-addon">
                    			<span class="glyphicon glyphicon-calendar"></span>
               				 </span>
						</div>
						<label for="number">第一位数 </label><input class="form-control inputText" type="text" id="oneColumn" />
						<label for="number">第二位数 </label><input class="form-control inputText" type="text" id="twoColumn" />
						<label for="number">第三位数 </label><input class="form-control inputText" type="text" id="threeColumn" />
						<label for="number">第四位数 </label><input class="form-control inputText" type="text" id="fourColumn" />
						<label for="number">第五位数 </label><input class="form-control inputText" type="text" id="fiveColumn" />
						<label for="number">第六位数 </label><input class="form-control inputText" type="text" id="sixColumn" />
						<label for="number">第七位数 </label><input class="form-control inputText" type="text" id="sevenColumn" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="edit()">提交更改</button>
			</div>
		</div>
	</div>
</div>





<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal2" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel2">新增</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<div class="form-group">
						<label for="number">期数 </label><input class="form-control inputText" type="text" id="numberId2" />
						<label for="number">开奖日期 </label>
						<div class='input-group date'  style="width: 200px;"  id='datetimepicker4' >
						<input type='text' class="form-control" id='createTime2' />
						<span class="input-group-addon">
                    			<span class="glyphicon glyphicon-calendar"></span>
               				 </span>
						</div>
						<label for="number">第一位数 </label><input class="form-control inputText" type="text" id="oneColumn2" />
						<label for="number">第二位数 </label><input class="form-control inputText" type="text" id="twoColumn2" />
						<label for="number">第三位数 </label><input class="form-control inputText" type="text" id="threeColumn2" />
						<label for="number">第四位数 </label><input class="form-control inputText" type="text" id="fourColumn2" />
						<label for="number">第五位数 </label><input class="form-control inputText" type="text" id="fiveColumn2" />
						<label for="number">第六位数 </label><input class="form-control inputText" type="text" id="sixColumn2" />
						<label for="number">第七位数 </label><input class="form-control inputText" type="text" id="sevenColumn2" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="add()">提交更改</button>
			</div>
		</div>
	</div>
</div>
<div class="alert"></div>

<div id="wrapper">
	<div id="page-wrapper" style="height: 100%">
		<div class="panel panel-default">
			<div class="panel-body">
					<div class="form-group">
						<label for="number">数字搜索 </label>
						<input type="text" class="form-control" id="number" value="${number}" name="number" style="width: 200px;"/>
						<input id="page" value="${page}" name="page" style="display: none;" />
						<input id="pageSize" value="${pageSize}" name="pageSize"  style="display: none;" />

						<label>开始日期 </label>
						<div class='input-group date' id='datetimepicker1' style="width: 200px;">
							<input type='text' class="form-control" id="createTime3"/>
							<span class="input-group-addon">
                    			<span class="glyphicon glyphicon-calendar"></span>
               				 </span>
						</div>
						<label>结束日期 </label>
						<div class='input-group date' id='datetimepicker2' style="width: 200px;">
							<input type='text' class="form-control" id="createTime4" />
							<span class="input-group-addon">
                    			<span class="glyphicon glyphicon-calendar"></span>
               				 </span>
						</div>

						<button onclick="go()" class="btn btn-primary">查询</button>
						<button style="margin-left:10px" onclick="reset()" class="btn btn-primary">清空</button>
						<button style="margin-left:10px" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">新增</button>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<table class="table table-bordered table-striped" id="detailData" style="text-align: center;">
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default" style="text-align: right">
					<button class="btn btn-primary" onclick="up()">上一页</button>
					<button class="btn btn-primary" onclick="down()">下一页</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/jquery-ui.min.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="/static/js/metisMenu.min.js"></script>
<script src="/static/js/jquery.dataTables.min.js"></script>
<script src="/static/js/dataTables.bootstrap.min.js"></script>
<script src="/static/js/sb-admin-2.js"></script>
<script type="text/javascript" src='/static/js/stopExecutionOnTimeout.js?t=1'></script>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript" src="/static/js/Particleground.js"></script>
<script type="text/javascript" src="/static/js/Treatment.js"></script>
<script type="text/javascript" src="/static/js/jquery.mockjax.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.24.0/moment-with-locales.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'YYYY-MM-DD',
			locale: moment.locale('zh-cn')
		});
		$('#datetimepicker2').datetimepicker({
			format: 'YYYY-MM-DD',
			locale: moment.locale('zh-cn')
		});
		$('#datetimepicker3').datetimepicker({
			format: 'YYYY-MM-DD',
			locale: moment.locale('zh-cn')
		});
		$('#datetimepicker4').datetimepicker({
			format: 'YYYY-MM-DD',
			locale: moment.locale('zh-cn')
		});
	});

	/**
	 * 弹出式提示框，默认1.2秒自动消失
	 * @param message 提示信息
	 * @param style 提示样式，有alert-success、alert-danger、alert-warning、alert-info
	 * @param time 消失时间
	 */
	var prompt = function (message, style, time)
	{
		style = (style === undefined) ? 'alert-success' : style;
		time = (time === undefined) ? 1200 : time;
		$('<div>')
				.appendTo('body')
				.addClass('alert ' + style)
				.html(message)
				.show()
				.delay(time)
				.fadeOut();
	};

	// 成功提示
	var success_prompt = function(message, time)
	{
		prompt(message, 'alert-success', time);
	};

	// 失败提示
	var fail_prompt = function(message, time)
	{
		prompt(message, 'alert-danger', time);
	};

	// 提醒
	var warning_prompt = function(message, time)
	{
		prompt(message, 'alert-warning', time);
	};

	// 信息提示
	var info_prompt = function(message, time)
	{
		prompt(message, 'alert-info', time);
	};



	function add(){
		console.log("==============add===============");
		var numberId2=document.getElementById("numberId2").value;
		var createTime2=document.getElementById("createTime2").value;
		var oneColumn2=document.getElementById("oneColumn2").value;
		var twoColumn2=document.getElementById("twoColumn2").value;
		var threeColumn2=document.getElementById("threeColumn2").value;
		var fourColumn2=document.getElementById("fourColumn2").value;
		var fiveColumn2=document.getElementById("fiveColumn2").value;
		var sixColumn2=document.getElementById("sixColumn2").value;
		var sevenColumn2=document.getElementById("sevenColumn2").value;
		if((numberId2==null || numberId2=='' || numberId2==undefined) ||
				(createTime2==null || createTime2=='' || createTime2==undefined) ||
				(oneColumn2==null || oneColumn2=='' || oneColumn2==undefined) ||
				(twoColumn2==null || twoColumn2=='' || twoColumn2==undefined) ||
				(threeColumn2==null || threeColumn2=='' || threeColumn2==undefined) ||
				(fourColumn2==null || fourColumn2=='' || fourColumn2==undefined) ||
				(fiveColumn2==null || fiveColumn2=='' || fiveColumn2==undefined) ||
				(sixColumn2==null || sixColumn2=='' || sixColumn2==undefined) ||
				(sevenColumn2==null || sevenColumn2=='' || sevenColumn2==undefined)
		){
			warning_prompt("数据不完整,请检查参数并重新填写",2000);
			console.log("数据不完整,请检查参数并重新填写");
		}else{
			//发送请求进行新增
			var requestDataBean = {
				numberId:numberId2,
				createTime:createTime2,
				oneColumn:oneColumn2,
				twoColumn:twoColumn2,
				threeColumn:threeColumn2,
				fourColumn:fourColumn2,
				fiveColumn:fiveColumn2,
				sixColumn:sixColumn2,
				sevenColumn:sevenColumn2
			};
			console.log("add - requestDataBean: "+JSON.stringify(requestDataBean));
			var url="/maima/addMaiMa";
			AjaxPost(url, requestDataBean,
					function () {
						//ajax加载中
					},
					function (data) {
						console.log("========数据获取成功======");
						if(data!=null && data.Code!=null && data.Code==200 && data.Status!=null && data.Status=="ok"){
							success_prompt(data.Msg,2000);
							go();//flush
						}else {
							fail_prompt(data.Msg,2000);
							AjaxErro(data);
						}
					})
		}
	}
	function edit(){
		console.log("==============edit===============");
		var id=document.getElementById("id").value;
		var numberId2=document.getElementById("numberId").value;
		var createTime2=document.getElementById("createTime").value;
		var oneColumn2=document.getElementById("oneColumn").value;
		var twoColumn2=document.getElementById("twoColumn").value;
		var threeColumn2=document.getElementById("threeColumn").value;
		var fourColumn2=document.getElementById("fourColumn").value;
		var fiveColumn2=document.getElementById("fiveColumn").value;
		var sixColumn2=document.getElementById("sixColumn").value;
		var sevenColumn2=document.getElementById("sevenColumn").value;
		if((id==null || id=='' || id==undefined)||
				(numberId2==null || numberId2=='' || numberId2==undefined) ||
				(createTime2==null || createTime2=='' || createTime2==undefined) ||
				(oneColumn2==null || oneColumn2=='' || oneColumn2==undefined) ||
				(twoColumn2==null || twoColumn2=='' || twoColumn2==undefined) ||
				(threeColumn2==null || threeColumn2=='' || threeColumn2==undefined) ||
				(fourColumn2==null || fourColumn2=='' || fourColumn2==undefined) ||
				(fiveColumn2==null || fiveColumn2=='' || fiveColumn2==undefined) ||
				(sixColumn2==null || sixColumn2=='' || sixColumn2==undefined) ||
				(sevenColumn2==null || sevenColumn2=='' || sevenColumn2==undefined)
		){
			warning_prompt("数据不完整,请检查参数并重新填写",2000);
			console.log("数据不完整,请检查参数并重新填写");
		}else{
			//发送请求进行新增
			var requestDataBean = {
				id:id,
				numberId:numberId2,
				createTime:createTime2,
				oneColumn:oneColumn2,
				twoColumn:twoColumn2,
				threeColumn:threeColumn2,
				fourColumn:fourColumn2,
				fiveColumn:fiveColumn2,
				sixColumn:sixColumn2,
				sevenColumn:sevenColumn2
			};
			console.log("edit - requestDataBean: "+JSON.stringify(requestDataBean));
			var url="/maima/editMaiMa";
			AjaxPost(url, requestDataBean,
					function () {
						//ajax加载中
					},
					function (data) {
						console.log("========数据获取成功======");
						if(data!=null && data.Code!=null && data.Code==200 && data.Status!=null && data.Status=="ok"){
							success_prompt(data.Msg,2000);
							go();//flush
						}else {
							fail_prompt(data.Msg,2000);
							AjaxErro(data);
						}
					})
		}
	}
	function reset(){
		document.getElementById("number").value="";
		document.getElementById("createTime3").value="";
		document.getElementById("createTime4").value="";
	}
	function show(obj,id){
		var value0 = $(obj).parents("td").parents("tr").children("td").eq(0).text();
		var value1 = $(obj).parents("td").parents("tr").children("td").eq(1).text();
		var value2 = $(obj).parents("td").parents("tr").children("td").eq(2).text();
		var value3 = $(obj).parents("td").parents("tr").children("td").eq(3).text();
		var value4 = $(obj).parents("td").parents("tr").children("td").eq(4).text();
		var value5 = $(obj).parents("td").parents("tr").children("td").eq(5).text();
		var value6 = $(obj).parents("td").parents("tr").children("td").eq(6).text();
		var value7 = $(obj).parents("td").parents("tr").children("td").eq(7).text();
		var value8 = $(obj).parents("td").parents("tr").children("td").eq(8).text();
		console.log("value0: "+value0+" value1: "+value1+" value2: "+value2+" value3: "+value3+" value4: "+value4+" value5: "+value5+" value6: "+value6+" value7: "+value7+" value8: "+value8);
		document.getElementById("id").value=id;
		document.getElementById("numberId").value=value0;
		document.getElementById("createTime").value=value1;
		document.getElementById("oneColumn").value=value2;
		document.getElementById("twoColumn").value=value3;
		document.getElementById("threeColumn").value=value4;
		document.getElementById("fourColumn").value=value5;
		document.getElementById("fiveColumn").value=value6;
		document.getElementById("sixColumn").value=value7;
		document.getElementById("sevenColumn").value=value8;
	}

	function Map() {
		/** 存放键的数组(遍历用到) */
		this.keys = new Array();
		/** 存放数据 */
		this.data = new Object();

		/**
		 * 放入一个键值对
		 * @param {String} key
		 * @param {Object} value
		 */
		this.put = function(key, value) {
			if(this.data[key] == null){
				this.keys.push(key);
			}
			this.data[key] = value;
		};

		/**
		 * 获取某键对应的值
		 * @param {String} key
		 * @return {Object} value
		 */
		this.get = function(key) {
			return this.data[key];
		};

		/**
		 * 删除一个键值对
		 * @param {String} key
		 */
		this.remove = function(key) {
			this.keys.remove(key);
			this.data[key] = null;
		};

		/**
		 * 遍历Map,执行处理函数
		 *
		 * @param {Function} 回调函数 function(key,value,index){..}
		 */
		this.each = function(fn){
			if(typeof fn != 'function'){
				return;
			}
			var len = this.keys.length;
			for(var i=0;i<len;i++){
				var k = this.keys[i];
				fn(k,this.data[k],i);
			}
		};

		/**
		 * 获取键值数组(类似<a href="http://lib.csdn.net/base/java" class='replace_word' title="Java 知识库" target='_blank' style='color:#df3434; font-weight:bold;'>Java</a>的entrySet())
		 * @return 键值对象{key,value}的数组
		 */
		this.entrys = function() {
			var len = this.keys.length;
			var entrys = new Array(len);
			for (var i = 0; i < len; i++) {
				entrys[i] = {
					key : this.keys[i],
					value : this.data[i]
				};
			}
			return entrys;
		};

		/**
		 * 判断Map是否为空
		 */
		this.isEmpty = function() {
			return this.keys.length == 0;
		};

		/**
		 * 获取键值对数量
		 */
		this.size = function(){
			return this.keys.length;
		};

		/**
		 * 重写toString
		 */
		this.toString = function(){
			var s = "{";
			for(var i=0;i<this.keys.length;i++,s+=','){
				var k = this.keys[i];
				s += k+"="+this.data[k];
			}
			s=s.substr(0,s.length-1);
			s+="}";
			return s;
		};
	}

	//上一页
	function up(){
		var upPage=document.getElementById("page").value;
		var pageSize=document.getElementById("pageSize").value;
		var number=document.getElementById("number").value;
		if(upPage=='' || upPage==undefined || upPage==null || upPage<1){
			upPage=0;
		}else{
			upPage=upPage*1-1;
		}
		if(upPage<0){
			upPage=0;
		}
		var requestDataBean = {page:upPage,pageSize:pageSize*1,number:number};
		console.log("上一页 - requestDataBean: "+JSON.stringify(requestDataBean));
		getData(requestDataBean);
	}
	//下一页
	function down(){
		var downPage=document.getElementById("page").value;
		var pageSize=document.getElementById("pageSize").value;
		var number=document.getElementById("number").value;
		if(downPage=='' || downPage==undefined || downPage==null){
			downPage=0;
		}else{
			downPage=downPage*1+1;
		}
		var requestDataBean = {page:downPage,pageSize:pageSize*1,number:number};
		console.log("下一页 - requestDataBean: "+JSON.stringify(requestDataBean));
		getData(requestDataBean);
	}
	//查询
	function go(){
		var page=document.getElementById("page").value;
		var pageSize=document.getElementById("pageSize").value;
		var number=document.getElementById("number").value;
		var createTime1=document.getElementById("createTime3").value;
		var createTime2=document.getElementById("createTime4").value;
		var requestDataBean = {page:page*1,pageSize:pageSize*1,number:number,createTime1:createTime1,createTime2:createTime2};
		console.log("数据查询 - requestDataBean: "+JSON.stringify(requestDataBean));
		getData(requestDataBean);
	}
	//给detailData表格填充数据
	 function fillDataToTable(arrays,sumArrays){
		var detailData=document.getElementById("detailData");
		detailData.innerHTML="";
		var row=detailData.insertRow(0);
		row.style.fontWeight="bolder";
		var cell0=row.insertCell(0);
		cell0.innerHTML="期数";
		var cell1=row.insertCell(1);
		cell1.innerHTML="开奖日期";
		var cell2=row.insertCell(2);
		 cell2.innerHTML="第一位数";
		var cell3=row.insertCell(3);
		 cell3.innerHTML="第二位数";
		var cell4=row.insertCell(4);
		cell4.innerHTML="第三位数";
		var cell5=row.insertCell(5);
		cell5.innerHTML="第四位数";
		var cell6=row.insertCell(6);
		cell6.innerHTML="第五位数";
		var cell7=row.insertCell(7);
		cell7.innerHTML="第六位数";
		var cell8=row.insertCell(8);
		cell8.innerHTML="第七位数";
		var cell9=row.insertCell(9);
		 cell9.innerHTML="操作";

		//分页数据插入
		for(var i=0;i<arrays.length;i++){
				var json=arrays[i];
				var row2=detailData.insertRow(i+1);
				var cell0=row2.insertCell(0);
				cell0.innerHTML=json.number;
				var cell1=row2.insertCell(1);
				cell1.innerHTML=json.createTime;
				var cell2=row2.insertCell(2);
				cell2.innerHTML=json.oneColumn;
				var cell3=row2.insertCell(3);
				cell3.innerHTML=json.twoColumn;
				var cell4=row2.insertCell(4);
				cell4.innerHTML=json.threeColumn;
				var cell5=row2.insertCell(5);
				cell5.innerHTML=json.fourColumn;
				var cell6=row2.insertCell(6);
				cell6.innerHTML=json.fiveColumn;
				var cell7=row2.insertCell(7);
				cell7.innerHTML=json.sixColumn;
				var cell8=row2.insertCell(8);
				cell8.innerHTML=json.sevenColumn;
				var cell9=row2.insertCell(9);
				cell9.innerHTML="<button onclick=\"show(this,"+json.id+")\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\" >编辑</button>";
		}

		//单排遗漏数计算  总排遗漏数的计算应该简单，即每一排当中的最小值index，与每一排的最大值index取出来放入集合最后遍历出最小index与最大indeax进行相减即可
		 var oneArray=[]; var twoArray=[]; var threeArray=[]; var fourArray=[]; var fiveArray=[]; var sixArray=[]; var sevenArray=[];
		 var sumArray=[];
		 //总数据 算遗漏数
		 for(var i=0;i<sumArrays.length;i++){
			 var json=sumArrays[i];
			 oneArray.push(json.oneColumn);
			 twoArray.push(json.twoColumn);
			 threeArray.push(json.threeColumn);
			 fourArray.push(json.fourColumn);
			 fiveArray.push(json.fiveColumn);
			 sixArray.push(json.sixColumn);
			 sevenArray.push(json.sevenColumn);
		 }
		 //插入7列数组数据
		 sumArray.push(oneArray);
		 sumArray.push(twoArray);
		 sumArray.push(threeArray);
		 sumArray.push(fourArray);
		 sumArray.push(fiveArray);
		 sumArray.push(sixArray);
		 sumArray.push(sevenArray);

		 var tr0=detailData.insertRow(arrays.length+1);
		 var td0=tr0.insertCell(0);
		 td0.colSpan=2;
		 td0.innerHTML="遗漏信息";
		 td0.style.fontWeight="bolder";
		 var td00=tr0.insertCell(1);
		 td00.colSpan=8;

		 var tr=detailData.insertRow(arrays.length+2);
		 var td=tr.insertCell(0);
		 td.style.fontWeight="bolder";
		 td.innerHTML="数字 | 单排最大遗漏数 | 总排最大遗漏数";
		 td.colSpan=2;

		 var numbers=document.getElementById("number").value.trim();
		 console.log("nums: "+numbers.split(","));
		 if(numbers==null || numbers=='' || numbers==undefined){//则遗漏信息、数字、单排最大遗漏数不进行显示
			 var td11=tr.insertCell(1);
			 td11.colSpan=8;
		 }else{
			 var nums=numbers.split(","); //则遗漏信息、数字、单排最大遗漏数进行显示 循环查询每个数值的单排 总排

			 for(var h=0;h<nums.length;h++){
			 	 var number=nums[h];//查询的数值
				 var singleMap=new Map();//关于该数值的单排遗漏数
				 var minIndexMap=new Map();//t每一列， 每一个单排当中的最小index
				 var maxIndexMap=new Map();//t每一列， 每一个单排当中的最大index

				 for(var t=0;t<sumArray.length;t++){ //当前7个数组当中都存有对应的数值,循环7次 // var arrays=[01,28,19,35,43,08,01,12,32,41];
					 var arrays2=sumArray[t];//单个数组
					 var map=new Map();

					 for(var i=0;i<arrays2.length;i++){
						 var element=arrays2[i];//数组当中具体的元素
						 map.put(element,0);//放入了该列当中所有不重复元素以及其世事变化的位移index+1

						 for(var n=0;n<arrays2.length;n++){
							 var key=arrays2[n];
							 if(key!=null && key==element){ //如果存在有当前循环的数组当中的取值与map当中的键相同的则重新赋值，重新赋值后进行break;
								 var y=i-n;
								 map.put(element,y);//此处的y没有减去当前自己的位数,所以当单排最大遗漏数查询的时候还需要-1

								 //每一组的min与max取出来之后的差也应该是该列的最大遗漏数+1，没有减1处理
								 if(element==number){ //当key不为空以及key与element相同的机会有多次，则每次赋值时进行比较看是否是最小值
									 if(minIndexMap.get(t)==null || minIndexMap.get(t)==undefined){ //取出的数值为空则说明还没有放入过值,则判断number与当前循环的element是否一致，如果一致则放入当前t与该element的位移index  //一般没有放过且第一次放的话即判断为最小index,后续无需再次判断了
										 minIndexMap.put(t,i);
									 }else{//从minIndexMap当中进行取出该t 如果当前number与当前的element相同，则放入当前element的位移,我觉得这种可能性没有
										if(minIndexMap.get(t)>i){
											minIndexMap.put(t,i);
										}
									 }
									 if(maxIndexMap.get(t)==null || maxIndexMap.get(t)==undefined){//取最大值的话可能性就有
										 maxIndexMap.put(t,i);
									 }else{
										 if(maxIndexMap.get(t)<i){
											 maxIndexMap.put(t,i);
										 }
									 }
								 }
								 break;
							 }else{
								 continue;
							 }
						 }
					 }
					 console.log("第"+t+"列当中，每一个元素的单排最大遗漏数："+map.toString());
					 if(map.get(number)!=null || map.get(number)!=undefined){
						 //取值则为当前该map.get(number)即当前列当中的单排最大遗漏数
						 singleMap.put(t,map.get(number)-1);//key t：哪一列， value 当前该数值在当前排t的最大遗漏数  map.get(number)需要减去1
					 }else{
						 //当前列取出number如果为空则说明当前列当中不存在有该数值，则替换为0
						 singleMap.put(t,'当前列不存在'+number);
					 }
				 }
				 console.log("minIndexMap: "+minIndexMap.toString());
				 console.log("maxIndexMap: "+maxIndexMap.toString());
				 var minIndex;
				 var mapArrays1=minIndexMap.entrys();
				 console.log("=======minIndexMap=======");
				 for(var i=0;i<mapArrays1.length;i++){ //当前列当中的min map数组
				 	console.log("[key]: "+mapArrays1[i].key+"  [value]: "+minIndexMap.get(mapArrays1[i].key));
					var value= minIndexMap.get(mapArrays1[i].key);
					 if(minIndex==undefined || minIndex==null){
						 minIndex=value;
					 }else{
						 if(minIndex>value){
						 	minIndex=value;
						 }
					  }
				 }
				 var maxIndex;
				 var mapArrays2=maxIndexMap.entrys();
				 console.log("=======maxIndexMap=======");
				 for(var i=0;i<mapArrays2.length;i++){ //当前列当中的min map数组
					 console.log("[key]: "+mapArrays2[i].key+"  [value]: "+maxIndexMap.get(mapArrays2[i].key));
					 var value=maxIndexMap.get(mapArrays2[i].key);
					 if(maxIndex==undefined || maxIndex==null){
						 maxIndex=value;
					 }else{
						 if(maxIndex<value){
						 	maxIndex=value;
						 }
					 }
				 }
				 var sumMaxNumber=maxIndex-minIndex-1;
				 console.log("[minIndex]: "+minIndex+" [maxIndex]: "+maxIndex+" [sumMaxNumber]: "+sumMaxNumber);

				 var mapArrays=singleMap.entrys();//循环之后获取得到singleMap,即单排数
				 if (h == 0) { //即第一个数值的时候填入行tr:arrays.length+2
					 td.innerHTML="数字: "+number+" | 单排最大遗漏数 | 总排最大遗漏数: "+sumMaxNumber;
					 for(var r=0;r<mapArrays.length;r++) {
						 var td22 = tr.insertCell(r+1);//当前行处于第一行
						 td22.innerHTML = singleMap.get(r);
					 }
					 tr.insertCell(mapArrays.length+1);
				 } else {
					 //新增行并插入
					 var newTr=detailData.insertRow(arrays.length+2+h);
					 var newTd=newTr.insertCell(0);
					 newTd.innerHTML="数字: "+number+" | 单排最大遗漏数 | 总排最大遗漏数: "+sumMaxNumber;
					 newTd.style.fontWeight="bolder";
					 newTd.colSpan=2;
					 for(var r=0;r<mapArrays.length;r++) {
						 var td22 = newTr.insertCell(r+1);//从第二行开始进行新增列
						 td22.innerHTML = singleMap.get(r);
					 }
					 newTr.insertCell(mapArrays.length+1);
				 }
				 console.log("================== 查询数值："+number+" END ==================");
			 }
		 }
	}
	//获取list接口数据
	function getData(requestDataBean){
		var url="/maima/findAllByParams";
		AjaxPost(url, requestDataBean,
			function () {
				//ajax加载中
			},
			function (data) {
				console.log("========数据获取成功======");
				//ajax返回 //认证完成
				if(data!=null){
					var arrays=JSON.parse(JSON.stringify(data.list));
					var sumArrays=JSON.parse(JSON.stringify(data.list2));
					document.getElementById("page").value=data.page;
					document.getElementById("pageSize").value=data.pageSize;
					if (arrays.length!=0) {
						//前端填充数据操作
						console.log("======= 表格填充数据 - fillDataToTable =======")
						fillDataToTable(arrays,sumArrays);
					} else {
						AjaxErro(data);
					}
				}
			})
	}
	//页面加载
	window.onload = function(){
		var requestDataBean = {page:0,pageSize:10};
		console.info("页面加载 - requestDataBean: "+JSON.stringify(requestDataBean));
		getData(requestDataBean);
	}
</script>
</body>
</html>































<%--

 //console.log(map.toString());
					 // var mapArrays=map.entrys();
					 // var maxValue=0;
					 // var maxKey=0;
					 // for(var i=0;i<mapArrays.length;i++){ //当前列当中的map数组
						//  var key=mapArrays[i].key;
						//  var value=map.get(key);
						 // console.log("key: "+key+" value:"+value);
					 // }
					 // console.log("单排最大遗漏数为 - key:"+maxKey+"  value:"+(maxValue-1));
					 //添加元素
					 // var td4=tr.insertCell(t+1);
					 // if(maxKey<=0){
						//  td4.innerHTML="-";
					 // }else{
						//  td4.innerHTML=maxKey;
					 // }
					 // var td5=tr2.insertCell(t+1);
					 // if((maxValue-1)<=0){
						//  td5.innerHTML="-";
					 // }else{
						//  td5.innerHTML=(maxValue-1);
					 // }


var tr=detailData.insertRow(arrays.length+2);
		 var td=tr.insertCell(0);
		 td.innerHTML="数字";
		 td.style.fontWeight="bolder";
		 td.colSpan=2;
		 var tr2=detailData.insertRow(arrays.length+3);
		 var td2=tr2.insertCell(0);
		 td2.innerHTML="单排最大遗漏数";
		 td2.style.fontWeight="bolder";
		 td2.colSpan=2;
		 var tr3=detailData.insertRow(arrays.length+4);
		 var td3=tr3.insertCell(0);
		 td3.innerHTML="总排最大遗漏数";
		 td3.style.fontWeight="bolder";
		 td3.colSpan=2;
// //当前7个数组当中都存有对应的数值
// for(var t=0;t<sumArray.length;t++){
//  // var arrays=[01,28,19,35,43,08,01,12,32,41];
//  var arrays=sumArray[t];//单个数组
//  console.log(arrays);
//  var flag=true;
//  var map=new Map();
//  for(var i=0;i<arrays.length;i++){ //循环10次
// 	 var element=arrays[i];//数组当中具体的元素
// 	 map.put(element,0);//放入的只有9个
//
// 	 for(var n=0;n<arrays.length;n++){ //循环10次
// 		 var key=arrays[n];
// 		 //console.log("当前遍历的element: "+element+"；所处index: "+i+"  map当中循环的element: "+key+"所处index: "+n)
// 		 if(key!=null && key==element){ //如果存在有当前循环的数组当中的取值与map当中的键相同的则重新赋值，重新赋值后进行下一次break;
// 			 var y=i-n;
// 			 //console.log("key: "+key+" element:"+element+" y: "+y);
// 			 map.put(element,y);//此处的y没有减去当前自己的位数,所以当单排最大遗漏数查询的时候还需要-1
// 			 break;
// 		 }else{
// 			 continue;
// 		 }
// 	 }
//  }
//  //console.log(map.toString());
//  var mapArrays=map.entrys();
//  var maxValue=0;
//  var maxKey=0;
//  for(var i=0;i<mapArrays.length;i++){
// 	 var key=mapArrays[i].key;
// 	 var value=map.get(key);
// 	 //console.log("key: "+key+" value:"+value);
// 	 if(maxValue<value){
// 		 maxValue=value;
// 		 maxKey=key;
// 	 }
//  }
//  console.log("单排最大遗漏数为 - key:"+maxKey+"  value:"+(maxValue-1));
//  //添加元素
//  var td4=tr.insertCell(t+1);
//  if(maxKey<=0){
// 	 td4.innerHTML="-";
//  }else{
// 	 td4.innerHTML=maxKey;
//  }
//  var td5=tr2.insertCell(t+1);
//  if((maxValue-1)<=0){
// 	 td5.innerHTML="-";
//  }else{
// 	 td5.innerHTML=(maxValue-1);
//  }
// }
--%>