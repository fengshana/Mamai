package com.maima.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Test2 {

    static Long minValue=new Long(-2);
    static Long maxValue=new Long(2);
    static{
        for(int i=0;i<30;i++){
            minValue=minValue*(2);
            maxValue=maxValue*2;
        }
        maxValue=maxValue-1;
        System.out.println("minValue: "+minValue);
        System.out.println("maxValue: "+maxValue);
    }

    /**
        给出一个32位的有符号整数
        你需要将这个整数中每位上的数字进行反转
     */
    public static int reverse(int x){
        String s=x+"";
        boolean flag=false;
        Long result=new Long(0);
        if(s.contains("-")){
            s=s.substring(1,s.length());
            flag=true;
        }
        StringBuilder stringBuilder=new StringBuilder();
        for(int i=s.length();i>0;i--){
            stringBuilder.append(s.substring(i-1,i));
        }
        if(flag){
            result=Long.parseLong("-"+stringBuilder.toString())<minValue?result:Long.parseLong("-"+stringBuilder.toString());
        }else{
            result=Long.parseLong(stringBuilder.toString())>maxValue?result:Long.parseLong(stringBuilder.toString());
        }
        return result.intValue();
    }

    public static void main(String[] args){

        int result=reverse(1563847412);
        System.out.println("result: "+result);


    }





}
